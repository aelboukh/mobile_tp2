package fr.uavignon.ceri.tp2;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.lifecycle.ViewModelStore;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp2.RecyclerAdapter.ViewHolder> {
    boolean isEnable = false;
    private int bookItemLayout;
    private List<Book> bookList;
    private ListViewModel viewModel;
    private static final String TAG = fr.uavignon.ceri.tp2.RecyclerAdapter.class.getSimpleName();

    public RecyclerAdapter(int layoutId) {
        bookItemLayout = layoutId;
    }

    public void setBookList(List<Book> books) {
        bookList = books;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int listPosition) {
        TextView itemTitle = viewHolder.itemTitle;
        TextView itemDetail = viewHolder.itemDetail;
        itemTitle.setText(bookList.get(listPosition).getTitle());
        itemDetail.setText(bookList.get(listPosition).getAuthors());

    }

    @Override
    public int getItemCount() {
        return bookList == null ? 0 : bookList.size();
    }

    public Book getBookAtPosition(int position) {
        return bookList.get(position);
    }

    private  void ClickItem(ViewHolder vHolder){
        long id = bookList.get((int) vHolder.getAdapterPosition()).getId() ;
//        viewModel.deleteBook(id);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);

           // int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    long id = RecyclerAdapter.this.bookList.get((int)getAdapterPosition()).getId() ;
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setBookNum(id);
                    Navigation.findNavController(v).navigate(action);


                }
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                     long bookId = RecyclerAdapter.this.bookList.get((int)getAdapterPosition()).getId() ;
                Snackbar.make(view, "Long Click detected on Book " + (bookId),
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    if (!isEnable){
                        ActionMode.Callback callback = new ActionMode.Callback() {
                            @Override
                            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                                MenuInflater menuInflater = actionMode.getMenuInflater();
                                menuInflater.inflate(R.menu.context_menu,menu);
                                return true;

                            }

                            @Override
                            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                                isEnable = true;
                                ClickItem(ViewHolder.this);
                                return true;
                            }

                            @Override
                            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                                viewModel.deleteBook(bookId);
                                actionMode.finish();
                                notifyDataSetChanged();
                                return true;
                            }

                            @Override
                            public void onDestroyActionMode(ActionMode actionMode) {

                                isEnable = false;
                                notifyDataSetChanged();

                            }
                        };
                        ((AppCompatActivity) itemView.getContext()).startActionMode(callback);
                    }else{

                    }
                    return true;
                }
            });

        }
    }

}