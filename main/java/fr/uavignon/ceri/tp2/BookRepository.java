package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;


public class BookRepository {
    private MutableLiveData<List<Book>> selectedBook =
            new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public void insertBook(Book newbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insert(newbook);
        });

    }

    public void upsert(Book newbook) {
        if (newbook == null){
            databaseWriteExecutor.execute(() ->{
            bookDao.insert(newbook);
        });}
        else{
            databaseWriteExecutor.execute(() -> {
                    bookDao.upsert(newbook);
            });
        }


    }
/*
    public void updateBook(Book updatedbook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.update(updatedbook);
        });
    }*/

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public void getBook(long id) {

        Future<List<Book>> fbooks = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbooks.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<List<Book>> getSelectedBook() {
        return selectedBook;
    }

}
