package fr.uavignon.ceri.tp2;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

@Dao
public interface BookDao {
/*
    @Insert
    void insertBook(Book book);

    @Update
    void updateBook(Book book);
*/
    @Insert
    void insert(Book book);

    @Update
    void upsert(Book book);
/*
    @Transaction
    public default void upsert(Book book) {
        long id = insert(book);
        if (id == -1) {
            insert(book);
        } else {
            update(book);
        }
    }*/
    @Query("DELETE FROM books WHERE bookId = :id")
    void deleteBook(long id);

    @Query("SELECT * FROM books WHERE bookId = :id")
    List<Book> getBook(long id);

    @Query("SELECT * FROM books")
    LiveData<List<Book>> getAllBooks();

    @Query("DELETE FROM books")
    void deleteAllBooks();
}
