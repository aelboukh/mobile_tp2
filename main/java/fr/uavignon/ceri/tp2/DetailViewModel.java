package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailViewModel extends AndroidViewModel
{
    private BookRepository repository;
    private MutableLiveData<List<Book>> selectedBook;

    public DetailViewModel(Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSelectedBook();
    }

    MutableLiveData<List<Book>> getSelectedBook() {
        return selectedBook;
    }
    public void getBook(long id) {
        repository.getBook(id);
    }
   /* public void insertOrUpdateBook(Book book) {
        repository.updateBook(book);
    }*/
    public void upsert(Book book) {
        repository.upsert(book);
    }
    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
}
