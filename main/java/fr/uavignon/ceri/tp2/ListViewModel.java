package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel
{
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;

    public ListViewModel(Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks = repository.getAllBooks();
    }

    LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }
    public void insertBook(Book book) {
        repository.insertBook(book);
    }
   /* public void updateBook(Book book) {
        repository.updateBook(book);
    }*/
    public void getBook(long id) {
        repository.getBook(id);
    }
    public void upsert(Book book) {
        repository.insertBook(book);
    }

    public void deleteBook(long id) {
        repository.deleteBook(id);
    }
}
