package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;
import java.util.Locale;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private DetailViewModel viewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        viewModel.getBook(args.getBookNum());
        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<List<Book>>() {
                    @Override
                    public void onChanged(@Nullable final List<Book> books) {

                        if (books.size() > 0) {
                            textTitle = (EditText) view.findViewById(R.id.nameBook);
                            textAuthors = (EditText) view.findViewById(R.id.editAuthors);
                            textYear = (EditText) view.findViewById(R.id.editYear);
                            textGenres = (EditText) view.findViewById(R.id.editGenres);
                            textPublisher = (EditText) view.findViewById(R.id.editPublisher);
                            textTitle.setText(books.get(0).getTitle());
                            textAuthors.setText(books.get(0).getAuthors());
                            textYear.setText(books.get(0).getYear());
                            textGenres.setText(books.get(0).getGenres());
                            textPublisher.setText(books.get(0).getPublisher());
                        }
                    }
                });

        Button updateButton = getView().findViewById(R.id.buttonUpdate);
        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Long id = args.getBookNum();

                String title = textTitle.getText().toString().trim();
                String author = textAuthors.getText().toString().trim();
                String year = textYear.getText().toString().trim();
                String genre = textGenres.getText().toString().trim();
                String publisher = textPublisher.getText().toString().trim();


               // if (!title.equals("") && !author.equals("") && !year.equals("") && !genre.equals("") && !publisher.equals(""))
                    if (!title.trim().isEmpty() && !author.trim().isEmpty() && !year.trim().isEmpty() && !genre.trim().isEmpty() &&
                            !publisher.trim().isEmpty()){

                    Book book = new Book(title,author,year,genre,publisher);
                    book.setId(id);
                    viewModel.upsert(book);

                    Snackbar.make(view, "Modification effectuée", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                        NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                                .navigate(R.id.action_SecondFragment_to_FirstFragment);

                } else {
                    Snackbar.make(view, "Merci de remplir tous les champs", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
        Button deleteButton = getView().findViewById(R.id.buttonDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.deleteBook(args.getBookNum());
                Snackbar.make(view, "Livre supprimé", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        // Get selected book
        /*
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        Book book = Book.books[(int)args.getBookNum()];

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        textTitle.setText(book.getTitle());
        textAuthors.setText(book.getAuthors());
        textYear.setText(book.getYear());
        textGenres.setText(book.getGenres());
        textPublisher.setText(book.getPublisher());
*/
        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}